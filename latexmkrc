$quote_filenames = 0;

$lualatex = 'lualatex --recorder --shell-escape -halt-on-error %O %S';
$xelatex = 'xelatex --recorder --shell-escape -halt-on-error %O %S';
$pdflatex = 'pdflatex --recorder --shell-escape -halt-on-error %O %S';
$success_cmd = 'copy %D .\%R.pdf';
$pdf_mode = 4;
$postscript_mode = $dvi_mode = 0;
$out_dir = ".build";

ensure_path( 'TEXINPUTS', '.\packages\\' );

