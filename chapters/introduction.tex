\chapter{Introduction}
The goal of this thesis is to explain and proof the paper \cite{Gelfand1950}. In this paper Gelfand and Tsetlin give a constructive way of classifying all finite dimensional irreducible representations of the Lie-Algebra $\mathfrak{gl}_n(K)$. We already have such a classification from Cartan (\ref{TODO}) but it only gives us the existence. This is where the formulas from the paper are very useful, because they explicitly give the operations of the representation.

\section{Notation}
In this thesis we will use the following notations:
\begin{enumerate}[label=(\roman*)]
    \item $e_{i,j}$ is the $n\times n$ matrix which has a $1$ at $(i,j)$ and $0$ everywhere else.
    \item $K_n$ is the Lie-Algebra $\mathfrak{gl}_n(K)$ with the Lie-Bracket $[A,B] \coloneqq AB - BA$ for $A,B \in \mathfrak{gl}_n(K)$
    \item Let $\varphi$ a representation then $E_{i,j} \coloneqq \varphi(e_{i,j})$
\end{enumerate}

\section{Basics}
First let us define a representation.
\begin{definition}
    Let $L$ be a Lie-Algebra, $V$ a vectorspace. Then $\varphi: L \to \mathfrak{gl}(V)$ is called a representation of $L$ if it is a Lie-Algebra homomorphism, i.e. a vectorspace homomorphism and 
    $$
    \forall x,y \in L : \varphi( [x, y] ) = [ \varphi(x), \varphi(y) ]
    $$
\end{definition}

\section{Setup}
To work with the representations as often in mathematics we try to consider them as matrices. Because of this we will formulate the following goal.
\subsection{The Goal}\label{goal}
Let $\varphi$ an irreducible order $N$ representation of $K_n$. Now the goal is to find all $n^2$ matrices $E_{i,j} \in K^{N \times N}$. These then define the representation completely because the $e_{i,j}$ are a basis of $\mathfrak{gl}_n(K)$ and so with the $E_{i,j}$ matrices we have the whole morphism.\\
\subsection{Cartan}
We already know that we can classify the finite dimensional irreducible representations of $K_n$ by just using cartan or Lemma \ref{lma:any_rep_iso}. So the reader might ask why we do all this other operations when the goal itself should already be reached. \\
The answer is that even if one takes a highest weight vector and then knows that there has to exist a representation for it, one does now know how this representation operates. This exact thing is the more precise goal of this thesis. We will give explicit operations which, when used with the right basis, define all operations precisely, as well as give a good idea how any given finite dimensional irreducible representation might operate.

\section{First Theorems}
We will now give a theorem and its proof which is given in the beginning of \cite{Gelfand1950}.
\begin{theorem}
    The following properties hold for any representation of $K_n$:\\
    $\forall 1 \leq i,j,k,l \leq n$:
    \begin{enumerate}[label=(\roman*)]
        \item $i \neq k: [E_{i,j}, E_{j,k}] = E_{i,k}$
        \item $[E_{i,j}, E_{j,i}] = E_{i,i} - E_{j,j}$
        \item $i \neq l, j \neq k: [E_{i,j}, E_{k,l}] = 0$
    \end{enumerate}
\end{theorem}
\begin{proof}
    Let $\varphi$ be any representation of $K_n$.\\
    First we observe that:
    $$
    \forall j \neq k : e_{i,j} \cdot e_{k,l} = 0
    $$
    and
    $$
    e_{i,j} \cdot e_{j,k} = e_{i,k}
    $$
    \begin{enumerate}[label=(\roman*)]
        \item Let $i \neq k$.
            \begin{align*}
                [E_{i,j}, E_{j,k}] &= [\varphi(e_{i,j}), \varphi(e_{j,k})]\\
                    &= \varphi([e_{i,j}, e_{j,k}])\\
                    &= \varphi(e_{i,j}e_{j,k} - e_{j,k}e_{i,j})\\
                    &= \varphi(e_{i,j}e_{j,k})\\
                    &= \varphi(e_{i,k})\\
                    &= E_{i,k}
            \end{align*}
        \item 
            \begin{align*}
                [E_{i,j}, E_{j,i}] &= [\varphi(e_{i,j}), \varphi(e_{j,i})]\\
                    &= \varphi([e_{i,j}, e_{j,i}])\\
                    &= \varphi(e_{i,j}e_{j,i} - e_{j,i}e_{i,j})\\
                    &= \varphi(e_{i,i} - e_{j,j})\\
                    &= \varphi(e_{i,i}) - \varphi(e_{j,j})\\
                    &= E_{i,i} - E_{j,j}
            \end{align*}
        \item Let $i \neq l, j \neq k$
            \begin{align*}
                [E_{i,j}, E_{k,l}] &= [\varphi(e_{i,j}), \varphi(e_{k,l})]\\
                    &= \varphi([e_{i,j}, e_{k,l}])\\
                    &= \varphi(e_{i,j}e_{k,l} - e_{k,l}e_{i,j})\\
                    &= 0
            \end{align*}
    \end{enumerate}
\end{proof}

With the following theorem we make the construction a bit easier by reducing the number of matrices that we need to explicitly construct by implying them.
\begin{theorem}\label{thm:Eij}
    The set $\{ E_{i-1, i}, E_{i,i}, E_{i,i-1} \}$ already defines all other $E_{p,q}$
\end{theorem}
\begin{proof}
    Let $1 \leq p,q \leq n$, w.l.o.g. $p \leq q$, $n \coloneqq q-p$\\
    We make an induction over $n$.\\
    For the case $n=0$ and $n=1$ $E_{p,q}$ is in the set of the theorem.\\
    Now assume that $n>1$ and the theorem holds for all $m < n$.\\
    \begin{align*}
        E_{p,q} &= [E_{p, p+1}, E_{p+1, q}]\\
            &= E_{p, p+1}E_{p+1, q} - E_{p+1, q}E_{p, p+1}
    \end{align*}
    But because $(p+1)-p = 1 < n$ and $q-(p+1) = n - 1 < n$ all parts are already defined.
\end{proof}

